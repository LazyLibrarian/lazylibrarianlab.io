Lazylibrarian opds server default address is `http://address:port/opds`  
The server has been tested with a small number of opds readers including
Aldiko, Moon+, FBReader, PocketBook on android

Not all features are available on all readers.

NOTE - most readers seem to require port 80 for opds, so you may need to run lazylibrarian through a transparent proxy.

If you are using opds authentication some readers will fail to display images
and searches may not return results if the reader only provides authentication for lists.
We do not have a work-around for this yet. If it bugs you either disable authentication or
switch to a more compliant reader.

Some readers do not paginate long lists correctly, there are two work-arounds in lazylibrarian
to help with this.  
  
* You can increase the number of items per page in lazylibrarian config.
Readers that do not paginate will only show the first page, but it could be a long page.  
* Aldiko has a known, long-standing pagination bug, so we disable pagination completely
if we detect that reader is being used.  

The opds page contents are variable depending on your library and the reader being used. 
  
* No genre option if there are no genres in your library   
* If your reader can't display pdf you may not see any magazines (not all readers are that smart)    
* The "recent" options (eg recent ebooks) are a paginated full list of *all* the books, sorted in most
recently added order    
* If you provide your userid in the opds feed name `http://address:port/opds?user=123456` additional options 
will appear in the opds lists with your Read and To-Read lists (if you use them).
Your userid is displayed on your lazylibrarian profile page.  
