#Notifications
<img src="../assets/screenshots/config_notifications.png" width="800">

For all the notifiers, Notify on Snatch sends a message whenever a book/magazine is sent to one of the
downloaders. Notify on Download sends a message when a snatched book/magazine has been successfully
downloaded and added to the library. Some notifiers have the ability to send a test message to check
the notifications work. For notifiers that can send to multiple devices,
the test message will return a list of valid device IDs.

## Enable Twitter Notifications
* Notify on Snatch
* Notify on Download
* Request Authorisation
* Twitter Key
* Verify Key
* Test Twitter

## Enable Boxcar Notifications
* Notify on Snatch
* Notify on Download
* Boxcar Token

## Enable Pushbullet Notifications
* Notify on Snatch
* Notify on Download
* Pushbullet API
* Device ID
* Test Pushbullet

## Enable Pushover Notifications
* Notify on Snatch
* Notify on Download
* Pushover API
* Keys
* Device
* Priority
* Test Pushover

## Enable AndroidPN
* Notify on Snatch
* Notify on Download
* Broadcast
* AndroidPN Notification URL
* Username
* Test AndroidPN

## Enable Email Notification
This is a special notifier, the notifier email settings are necessary for emailing users if user accounts are enabled, even if regular email notifications are disabled. 
The settings are used for emailing login details, replies to user queries, and for emailing books/magazines to users.  
The default emails are plain text, but some example email templates are included with lazylibrarian, for example to send html emails.   
To use the templates, move them into your lazylibrarian data folder (alongside the database file) and call them filetemplate.text and logintemplate.text  
example.logintemplate.text is used to send a user their lazylibrarian login details (password, username). You might like to expand this to send the user the web location of your lazylibrarian server, and maybe pretty it up with some html. There are a few variables passed to the template, {username} {password} {permission} and the template will be rejected if it does not include these. {username} and {password} are self-explanatory, the {permission} variable will return one of (admin/guest/friend/custom)  
example.filetemplate.text is used to email files to the user. Variables included are {name} {method} {link}  
{name} is the title of the book/magazine without extension.  
{method} is one of (is attached/is unavailable/is not available/is not available in requested format/is too large to email).  
{link} is a link to an external download site (if enabled, and the file is too large to attach to an email).  
example.html.filetemplate.text is a more elaborate template that sends a html email, optionally with an embedded {cid:logo}  
* Notify on Snatch  
* Notify on Download  
* Email From address  
* Email To address  
* SMTP Server address  
* SMTP user and password  
* SMTP port  
* Use SSL  
* Use TLS  
* Test Email  

## Enable Custom Notification
This is a special notifier, which is a script to run. There are some example scripts with lazylibrarian. You can use it, for example, to notify a system we don't support yet. There is an example script in python and another in bash that shows how the notifiers work. There is also example_ebook_convert.py that can be used as a notifier to make sure you have a copy of both mobi and epub for all new downloads. It uses calibre ebook-convert to create one format from another. Use it as a base for your own notifiers. The notifiers are passed a list of database columns for a book or magazine, and details of the download source. See the examples for full details.

If you create any other notifiers that you think might be useful to others, please upload them!

## Apprise Notifiers
This is a new libray, not bundled with lazylibrarian at present, but it is easily installed using pip  
* pip install apprise  
  
This duplicates some of the built in notifiers, but adds many more...  
Boxcar Discord Emby Faast Gnome Growl IFTTT Kodi Join Matrix Mattermost Prowl Pushalot PushBullet PushJet Pushover 
Rocket.Chat Slack Stride SuperToasty Telegram Twitter XBMC Windows and many email providers  
See the full list and usage instructions [here](https://github.com/caronc/apprise)  
  
Differences:  
1. The old notifiers for Twitter and Telegram have options to setup authorisation, apprise needs you to already be authorised  
2. The old email notifier is used for messaging user accounts and emailing book attachments so may still be needed  
  
