If you have user accounts enabled each user has a Calibre_Read and Calibre_To_Read column setting in config. 
These can be used to sync user bookmarks to custom columns in calibre, see calibre documentation for how the custom columns work.
Setting the column name for each user can only be done by an administrator as the column names should be unique to prevent one user overriding anothers details.

The columns are synced from a button on the lazylibrarian "Manage eBooks" page, users can only sync their own books.