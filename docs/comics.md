#Comics
<img src="../assets/screenshots/comics_main.png" width="800">


##Menu
The menu bar has five buttons <img src="../assets/screenshots/comics_menu.png" width="500">

* **[Search]** searches your download providers for all comics marked "Wanted"
* **[PostProcessor]** adds any comics in your download directory (defined in config) to your library (also defined in config)
* **[Recent Issues]** shows a "wall" of cover images from the comics in your library, most recently added first. Clicking on a cover image in the wall will open the comic.
* **[RSS]** RSS feed of recent downloads.

To the right of the buttons is a comic selector this is used to search and add new comics. 

Below you will find the usual status selector, toggles, rows per page and results filter as described in the homepage.

* **[Active]** Keep trying to find new issues of this comic(s) and download new issues
* **[Paused]** Stop looking for new issues of this comic.
* **[Reset]** Look for old issues (not just newer than latest issue)
* **[Remove]** remove selected comic(s)from LazyLibrarian database. Does not delete local files.
* **[Delete]** delete comic(s) selected from database and deletes local files as well.


In the table, clicking on the comic cover image shows a larger image. The Status column has either **[Show]** or **[Open]** clickable buttons. **[Show]** takes you to a page with all downloaded issues of that comic, **[Open]** (when configured) allows you to open a comic if you only have one issue downloaded.
