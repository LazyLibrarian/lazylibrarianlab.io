If user accounts are enabled there are subscription options available in some of the dropdowns.  

Admins can add/remove users as subscribers to wishlists  
Users can add/remove themselves as subscribers to series/author/magazine/comic and toggle whether to see all items or only ones to which they are subscribed  

Example:  
If you are a subscriber to an author, any new books downloaded for that author will be emailed to you.  
If you subscribe to a magazine you will be emailed all new issues, etc.  

If the document is larger than the email size limit set in lazylibrarian, or if the book is not available in your preferred format you will instead get an email telling you it is available for download from the server.  
