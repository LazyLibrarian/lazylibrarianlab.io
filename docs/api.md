# API

Lazylibrarian now has a basic api. To use it...
http://host:port/api?apikey=your_api_key&cmd=help

This will list all available commands and any extra parameters required.  
Simple commands just return OK, more complex commands return results as JSON so you probably want to use json.loads() to read the results.  
Time consuming commands such as library scanning and updating active authors return OK straight away, and then run in the background as a separate thread. If you need to know when they have completed, monitor the log,  or use the optional &wait parameter. Some proxy servers will timeout if the wait is too long. 
  
Parameters passed to the api should be in urllib.quote_plus style, eg name=Tom+Holt, not name="Tom Holt" or name=Tom%20Holt  
and be aware that Goodreads is very fussy about initials, and returns strange/irrelevant/random results if you don't get it right.  
It doesn't like initials followed by spaces, examples below are quote_plus encoded...  
eg "J+R+R+Tolkien" fails, it needs to be "J.+R.+R.+Tolkien" or "J.R.R.Tolkien" or just "Tolkien"  
but it DOES need spaces if not initials eg "Tom.Holt" fails, but "Tom+Holt" works  
  
Also if there are any functions you might like that aren't implemented, post a message on the issues page and I'll see what I can do.

####addAuthor:
&name= add author to database by name
####addAuthorID:
&id= add author to database by AuthorID 
####addBook:
&id= add book details to the database 
####addMagazine:
&name= add magazine to database by name 
####authorUpdate:
update the oldest author, if any are overdue
####calibreList:
[&toread=] [&read=] get a list of books in calibre library 
####checkModules:
Check which library modules lazylibrarian is using 
####checkRunningJobs:
ensure all needed jobs are running 
####cleanCache:
[&wait] Clean unused and expired files from the LazyLibrarian caches 
####clearLogs:
clear current log 
####createMagCovers:
[&wait] [&refresh] create covers for magazines, optionally refresh existing ones 
####createMagCover:
&file= [&refresh] [&page=] create cover for magazine issue, optional page number 
####createPlaylist:
&id Create playlist for an audiobook
####deleteEmptySeries:
Delete any book series that have no members 
####dumpMonths:
Save installed monthnames to file 
####exportCSVwishlist:
[&wait] [&dir=] Export a CSV wishlist to named or alternate directory
####findAuthor:
&name= search goodreads/googlebooks for named author
####findBook:
&name= search goodreads/googlebooks for named book 
####forceActiveAuthorsUpdate:
[&wait] [&refresh] reload all active authors and book data, refresh cache 
####forceAudioBookScan:
[&wait] [&remove] [&dir=] [&id=] rescan whole or part audiobook library 
####forceBookSearch:
[&wait] [&type=eBook/AudioBook] search for all wanted books
####forceLibraryScan:
[&wait] [&remove] [&dir=] [&id=] rescan whole or part book library 
####forceMagSearch:
[&wait] search for all wanted magazines 
####forceProcess:
[&dir] [ignorekeepseeding] process books/mags in download or named dir 
####forceRSSSearch:
[&wait] search all entries in rss feeds   
####forceWishlistSearch:
[&wait] search all entries in wishlists
####getAllBooks:
list all books in the database 
####getAuthor:
&id= get author by AuthorID and list their books 
####getAuthorImages:
[&wait] get images for all authors without one 
####getAuthorImage:
&id= get an image for this author 
####getBookAuthors:
&id= Get list of authors associated with this book 
####getBookCovers:
[&wait] Check all books for cached cover and download one if missing 
####getBookCover:
&id= [&src=] fetch cover link from cache/cover/librarything/goodreads/google for BookID
####getDebug:
show debug log header 
####getHistory:
list history 
####getIndex:
list all authors 
####getIssues:
&name= list issues of named magazine 
####getLogs:
show current log 
####getMagazines:
list magazines 
####getModules:
show installed modules 
####getRead:
list read books for current user 
####getRSSFeed:
&feed= [&limit=] show rss feed entries 
####getSeriesAuthors:
&id= Get all authors for a series and import them 
####getSeriesMembers:
&id= Get list of series members using SeriesID 
####getSnatched:
list snatched books 
####getToRead:
list to-read books for current user 
####getVersion:
show lazylibrarian current/git version 
####getWanted:
list wanted books 
####getWorkPage:
&id= Get url of Librarything BookWork using BookID 
####getWorkSeries:
&id= Get series from Librarything BookWork using BookID or GoodReads using WorkID 
####grFollowAll:
Follow all lazylibrarian authors on goodreads 
####grFollow:
&id= Follow an author on goodreads 
####grSync:
&status= &shelf= [&library=] Sync books with given status to a goodreads shelf
####grUnfollow:
&id= Unfollow an author on goodreads 
####help:
 list available commands. 
####ignoreAuthor:
&id= ignore author by AuthorID 
####includeAlternate:
[&wait] [&dir=] Include books from named or alternate folder and any subfolders 
####importAlternate:
[&wait] [&dir=] Import books from named or alternate folder and any subfolders 
####importCSVwishlist:
[&wait] [&dir=] Import a CSV wishlist from named or alternate directory 
####listIgnoredAuthors:
list all authors in the database marked ignored 
####listIgnoredBooks:
list all books in the database marked ignored
####listIgnoredSeries:
list all series in the database marked ignored 
####listMissingWorkpages:
list all books with errorpage or no workpage 
####listNoISBN:
list all books in the database with no isbn 
####listNoBooks:
list all authors in the database with no books  
####listNoDesc:
list all books in the database with no description  
####listNoGenre:
list all books in the database with no genre  
####listNoLang:
list all books in the database with unknown language  
####loadCFG:
reload config
####logMessage:
&level= &text= send a message to lazylibrarian logger
####moveBook:
&id= &toid= move one book to new author by BookID and AuthorID 
####moveBooks:
&fromname= &toname= move all books from one author to another by AuthorName 
####nameVars:
&id Show the name variables that would be used for a bookid 
####pauseAuthor:
&id= pause author by AuthorID 
####queueBook:
[&id= &type=eBook/AudioBook] mark book as Wanted default eBook 
####readCFG:
&name=&group= read value of config variable \name\ in section \group\ 
####refreshAuthor:
&name= [&refresh] reload author (and their books) by name optionally refresh cache 
####removeAuthor:
&id= remove author from database by AuthorID 
####removeMagazine:
&name= remove magazine and all of its issues from database by name 
####renameAudio:
&id Rename an audiobook using configured pattern 
####restart:
restart lazylibrarian 
####restartJobs:
restart background jobs, getRead:list read books for current user
####resumeAuthor:
&id= resume author by AuthorID 
####saveTable:
&table= Save a database table to a file 
####searchBook:
&id= [&wait] [&type=eBook/AudioBook] search for one book by BookID 
####searchItem:
&item= get search results for an item (author title, isbn) 
####setAllBookAuthors:
[&wait] Set all authors for all books from book workpages 
####setAllBookSeries:
[&wait] Set the series details from goodreads or librarything workpages 
####setAuthorImage:
&id= &img= set a new image for this author
####setAuthorLock:
&id= lock author name/image/dates 
####setAuthorUnlock:
&id= unlock author name/image/dates 
####setBookImage:
&id= &img= set a new image for this book 
####setBookLock:
&id= lock book details 
####setBookUnlock:
&id= unlock book details 
####setNoDesc:
[&refresh] set description for all books in the database without one, retry "No Description" entries on refresh  
####setNoGenre:
[&refresh] set genre for all books in the database without one, retry "Unknown" entries on refresh  
####setWorkID:
[&wait] [&bookids] Set WorkID for all books that dont have one, or bookids 
####setWorkPages:
[&wait] Set the WorkPages links in the database (unavailable at present, disabled at librarything)
####showCaps:
&provider= get a list of capabilities from a provider
####showJobs:
show status of background jobs 
####showMonths:
show installed monthnames
####showStats:
show database statistics
####showThreads:
show threaded processes 
####shutdown:
stop lazylibrarian 
####syncCalibreList:
[&toread=] [&read=] sync list of read/toread books with calibre 
####update:
update lazylibrarian 
####unqueueBook:
&id= [&type=eBook/AudioBook] mark book as Skipped default eBook 
####vacuum:
vacuum the database 
####writeCFG:
&name=&group=&value= set config variable \name\ in section \group\ to value
####writeAllOPF:
[&refresh] write out opf files for all books, optionally overwrite existing opf
####writeOPF:
&id= [&refresh] write out an opf file for a bookid, optionally overwrite existing opf
####preprocessAudio:
&dir= &author= &title= [&id=] [&tag] [&merge] preprocess an audiobook folder
####preprocessBook:
&dir= preprocess an ebook folder
####preprocessMagazine:
&dir= &cover= preprocess a magazine folder
####memUse:
memory usage of the program in kB
####cpuUse:
recent cpu usage of the program
####nice:
show current nice level
####nicer:
make a little nicer
####subscribe:
&user= &feed= subscribe a user to a feed
####unsubscribe:
&user= &feed= remove a user from a feed
####listAlienAuthors:
List authors not matching current book api
####listAlienBooks:
List books not matching current book api
####listNabProviders:
List all newznab/torznab providers
####listRSSProviders:
List all rss/wishlist providers
####listTorrentProviders:
List all torrent providers
####listIRCProviders:
List all irc providers
####listDirectProviders:
List all direct providers
####listProviders:
List all providers
####changeProvider:
&name= &xxx= Change values for an existing provider, valid parameters are shown in listProviders for the relevant provider type
####addProvider:
&type= &xxx= Add a new provider, type should be newznab,torznab,irc,rss,gen. Provide optional extra parameters or defaults are used as per last (empty) entry of each type shown in listProviders
####delProvider:
&name= Delete a provider by name
