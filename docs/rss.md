LazyLibrarian rss feed buttons may be available on ebook, magazine, audiobook pages,
if enabled in config and if user accounts are enabled.
the default address is `http://address:port/rssFeed?type=eBook&limit=10.xml`  
where the limit is how many recent items to return
and type is eBook AudioBook or Magazine.
Copy/paste this address into your rss reader.
In addition you can add your userid to get links to your preferred format to download
eg `http://address:port/rssFeed?type=eBook&limit=10&user=12345678.xml`  
There is an option in config to allow you to send the audiobook rss as an itunes compatible podcast. This works well with single file audiobooks. 
For multi-file audiobooks we currently only send the first chapter in the podcast link. 
Multi-file audiobooks are sent as a zip file if the podcast box is not enabled, which works with some players but not others.
The built in preprocessor options can be used to convert multi-file audiobooks to a single audio file using ffmpeg.
  
Your userid is displayed on your user profile page.
