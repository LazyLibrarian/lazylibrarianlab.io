#Installation

LazyLibrarian runs by default on port 5299 at http://localhost:5299

Linux / Mac OS X:

* Install Python 3 v3.5 or higher 
* Git clone/extract LL wherever you like
* Run "python LazyLibrarian.py -d" to start in daemon mode
* Fill in all the config (see docs for full configuration)


## Documentation:
* [website](https://lazylibrarian.gitlab.io/)

## Support/Issues
* [reddit](https://www.reddit.com/r/LazyLibrarian/)
* [issues](https://gitlab.com/LazyLibrarian/LazyLibrarian/issues)

## Tutorials
* [Docker (sasquatters)](http://sasquatters.com/lazylibrarian-docker/)
* [Config (sasquatters)](http://sasquatters.com/lazylibrarian-configuration/)
* [LazyLibrarian, Calibre, Calibre-Web, Booksonic Installation Guide v1.0 (gmhowell)](https://www.reddit.com/r/LazyLibrarian/comments/jw5e22/lazylibrarian_calibre_calibreweb_booksonic/)

For config options see the [configuration docs](/config_importing/).

## Updates
Auto updates are available via interface from master for git and source installs, and some docker images.  
If installed from a package (deb, rpm, snap, flatpak) please use your package manager to update.

## Packages
rpm deb flatpak and snap packages are no longer supported

* AUR package available here:
    * [AUR](https://aur.archlinux.org/packages/lazylibrarian/)
* QNAP LazyLibrarian is now available for the QNAP NAS via sherpa.
    * [QNAP](https://forum.qnap.com/viewtopic.php?f=320&t=132373v)

## Docker packages

* armhf version with calibre
    * [ARMHF](https://github.com/scambra/docker-lazylibrarian-calibre)
* armhf version and x64 version with optional calibre addon
    * [MULTIARCH](https://hub.docker.com/r/linuxserver/lazylibrarian/)
   
