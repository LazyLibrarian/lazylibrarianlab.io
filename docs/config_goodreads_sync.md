Goodreads has three default shelves per account, Read, To_Read, Reading  
and by default every book you tell goodreads about must appear exclusively on one of these shelves.  
You can create your own shelves to use in addition to the goodreads ones, or mark your shelves as exclusive too.
If you do not mark your shelves as exclusive, a book must still be on one exclusive shelf, but can also appear on any 
number of non-exclusive shelves.  
  
LazyLibrarian can sync up to 4 shelves. eBook and AudioBook, both Wanted and Owned (Owned means either Open/Have)  
  
If you only want to sync ebooks, and are not too fussy about "To Read" not meaning quite the same thing as "Wanted", 
and "Read" not meaning the same thing as "Owned", the simplest sync method is to use the existing GoodReads shelves.
Tell lazylibrarian to sync "Owned" to "Read" and to sync "Wanted" to "To Read"  
  
A more correct sync would involve using your own shelf names. LazyLibrarian will create the shelves for you.
The books you sync this way have to also appear on one of the original goodreads "exclusive" shelves, GoodReads 
puts them on "Read" by default. This does not seem to be controllable by LazyLibrarian.  
  
If you mark your own shelves as exclusive, each book must appear on one of your exclusive shelves *or* one of the goodreads ones.
LazyLibrarian does not automatically set the new shelves as exclusive, but you can log into your goodreads account to do this.  
  
In an ideal world we would be able to have a book marked as both "Owned" and "Read" but GoodReads shelves are designed as 
reading lists, not ownership lists, so the purposes are different. If you create your own shelves, you may just have to ignore 
the original goodreads ones being incorrect.  
  
The goodreads sync can be triggered from a button on the "Manage" page, or from a scheduled task set in lazylibrarian config.