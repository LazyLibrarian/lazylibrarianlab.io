#Comic Detail
<img src="../assets/screenshots/comics_detail.png" width="800">

Very similar to the comics page, same menu bar and options

The menu bar has four buttons <img src="../assets/screenshots/comic_detail_menu.png" width="500">

**[Search]** searches your download providers for all comics marked "Wanted"

**[Run Post Processor]** adds any comics in your download directory (defined in config) to your library (also defined in config)

**[LibraryScan]** will search your library and add all the comics it can find into your lazylibrarian database, creating cover images where possible.

**[Recent Issues]** shows a "wall" of cover images from the comics in your library, most recently added first. Clicking on a cover image in the wall will open the comic.

Below you will find a selector allowing you to **[Remove]** or **[Delete]** individual issues. Select the ones you want to perform an action on, and click **[Go]**. Remove just takes the issues out of the database, Delete also deletes the comic files. There are the usual toggles, rows per page and results filter as described in the homepage.

In the table, clicking on the comic cover image shows a larger image. The Select column has an **[Open]** clickable button to open the comic in your browser/reader.
