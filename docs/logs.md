#Logs
Very simple table, nothing to alter, just info.

##Menu

The menu bar has three or four buttons

<img src="https://imgur.com/nbhdzlE.png" width="300">

* **[Clear screen Log]**  
* **[Delete Logfiles]**   
* **[Toggle Detail/Summary]** show more or less info (columns) in the screen log
* **[Get Support Zip]** This option is only available if "redact logfiles" is enabled in lazylibrarian config, to ensure passwords etc are not included.

* To use the support zip for debugging, first go to the config page and make sure logging is set to DEBUG and the box "Redact logss saved to disc" is checked. 
  Other debug options allow focusing in on different areas. Leave these unchecked unless advised to enable them by lazylibrarian support.   
* Go and do whatever you need to recreate the error  
* Go back to the log page and press "Get support zip". It will create a zip file containing a redacted log and system/config info to assist in identifying the problem.  
* You can now turn debug logging off again if you want  
* Attach the zip file to your bug report.   

This page is intentionally oversized for small screens, but can be scrolled or columns toggled on/off as required
