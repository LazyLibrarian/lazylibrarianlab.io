#Categories
<img src="../assets/screenshots/config_categories.png" width="800">

You only need to alter category settings if auto-detection fails or returns incorrect values, or if you want to add additionl parameters. If you make any alterations, tick the lock button to prevent reloading values from the provider. API limit is not read from the provider and is not affected by lock.  
  
The "Extended Search" box can be used to add additional parameters. The first parameter should be 0 or 1 for extended search, followed by any other parameters eg 1&cachetime=2160  

