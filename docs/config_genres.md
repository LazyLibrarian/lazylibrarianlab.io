Genres

Genre support in lazylibrarian is not as comprehensive as we would like as
there does not seem to be a good source of info for us to use.  
Ideas/suggestions for improvement are welcome.

GoogleBooks has genres for many of it's books (not all), but only has one genre per book.
It also has some very strange choices for genre names.
GoodReads doesn't have true genres, but it has user named shelves, up to 100 per book, again not for all books.

If you are using the GoogleBooks interface in LazyLibrarian we use GoogleBooks genres.
Google limits us to 1000 hits per day, so it can take a while to get around all your books first time.
We update each authors books on refresh, but first time you load a new library we have to ignore genres
once that limit is reached.

If you are using the GoodReads interface we use the names of the most popular 4 user shelves per book.
We only count user shelves with commonly used names, default is 10 users who have that shelf name for that book.
Goodreads limits us to one call per second, so on a new library this can also take a while, and
if goodreads doesn't have a genre we try googlebooks as a fall-back, subject to the limits already mentioned.

The results are filtered and modified using a json file before adding to the database.
The filter lets you choose how many goodreads genres to keep per book (ignored for google, they only give one genre)
and allows you to ignore some genre names or change one name to another.

Filters

The initial filter file is example.genres.json  
Please do not modify the original or your modifications will be overwritten by lazylibrarian updates.
Copy the file into your data folder alongside your lazylibrarian.db and call it genres.json and modify the copy,
or edit the genres using the inbuilt editor in config which saves changes in the new location.
If you edit the json file manually and get the edit wrong so the file will not load you will get an error message
in the lazylibrarian startup log.

* genreLimit: default is 4, show the most popular 4 goodreads shelves for this book  
* genreUsers: default is 10, the shelf name must be used by at least 10 goodreads users for this book  
* genreExclude: do not include these genres in the database (eg my-books)  
* genreExcludeParts: do not include names with these parts (eg read-2018, want-to-read)  
* genreReplace: in pairs, replace the first name with the second so you can merge similar genre names  
(eg cookery becomes cooking, no point in having two similar genre names for the same books)

In addition to the json filter we try to filter out the author name, but we don't
filter character names or just author surname

You can also edit genres manually from each book "manual edit" page

Genres are displayed in lazylibrarian book lists, so can be used for filtering on those tables,
and appear as options in the opds server.
